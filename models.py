from peewee import *
import datetime
from wtforms.fields.html5 import *
import sqlite3 as sql

database = SqliteDatabase("flux.sqlite3")

class BaseModel(Model):
    class Meta:
        database = database

class User(BaseModel):
    name = CharField()
    surname = CharField()
    email = CharField()
    password = CharField()

class Flux(BaseModel):
    title = CharField()
    description = CharField()
    pubDate = DateTimeField(default=datetime.datetime.now)
    link = CharField()
    user = ForeignKeyField(User, backref="user")

def create_tables():
    with database:
        database.create_tables([Flux, User])

def drop_tables():
    with database:
        database.drop_tables([Flux, User])

        import sqlite3 as sql

def insertUser(name, password):
        con = sql.connect("flux.sqlite3")
        cur = con.cursor()
        cur.execute("INSERT INTO user (name,password) VALUES (?,?)", (name, password))
        con.commit()
        con.close()

def retrieveUsers():
        con = sql.connect("flux.sqlite3")
        cur = con.cursor()
        cur.execute("SELECT name, password FROM user")
        user = cur.fetchall()
        con.close()
        return user


def retrieveUsers(name, password):
    con = sql.connect("flux.sqlite3")
    cur = con.cursor()
    cur.execute("SELECT name, password FROM user", (name, password))
    user = cur.fetchall()
    con.close()
    return user

