# Projet_web-master-syndication

**PROJET WEB DYNAMIQUE** : *contributeurs* -> **LOUKHAL Maxime**, **Delletoile Antoine**

Ce projet est application web contenant: 
1. Une plateforme d'inscription
2. Une plateforme de connexion
3. Un formulaire de souscription à des flux
4. Ainsi qu'une page pour visualiser l'ensemble des flux auxquels a souscrit l'utilisateur

Le projet a été réalisé en python 3.8 à l'aide de flask et jina2 en grande partie, l'environnement virtual comprend : 

1. pytest | 5.3.5  
2. flask  | 1.1.1 
3. jinja2 | 2.11.1
4. requests| 2.23.0
5. peewee | 3.13.1 
6. faker  | 4.0.1 
7. awesome-slugify | 1.6.5
8. flask-wtf | 0.14.3
9. wtf-peewee | 3.0.0
10. bcrypt | 3.1.7
11. feedparser | 5.2.1
12. lxml | 4.5.0 | 
13. flask-login | 0.5.0 








