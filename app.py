import sqlite3
import click
import xml.etree.ElementTree as ET

from xml.etree import ElementTree

from flask import *
from flask_wtf import *
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

from models import *
from forms import *

import os


import click


from forms import *
from models import *
from peewee import *

app = Flask(__name__)
app.secret_key = 'Antoine'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)

@app.route('/', methods=['GET', 'POST', ])
def home():
    """Route vers la page d'accueil"""
    form = InscriptionForm()
    if form.validate_on_submit():
        user = User(
            name=form.name.data,
            surname=form.surname.data,
            email=form.email.data,
            password=generate_password_hash(form.password.data)
        )
        user.save()
    return render_template("index.html", form=form)


@app.route('/connexion', methods=['GET', 'POST'])
def connexion():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(email=form.email.data)
        user.is_active = True
        User.is_authenticated = True
        if user and check_password_hash(user.password, form.password.data):
            login_user(user)
            flash('Welcome')

            if user.is_authenticated:
                return redirect(url_for('flow'))
    return render_template("connexion.html", form=form)

@login_required
@app.route('/deconnexion')
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/flow', methods=['GET', 'POST'])
@login_required
def flow():
    tree = ET.parse('main_flux.xml')
    root = tree.getroot()

    return render_template("flow.html", root=root)

@app.route('/newflow', methods=['GET', 'POST'])
@login_required
def newflow():
    form = FluxForm()
    if form.validate_on_submit():
        flux = Flux(
            title=form.title.data,
            description=form.description.data,
            link=form.link.data,
            user_id=current_user.id
        )

        flux.save()

        tree = ET.parse('main_flux.xml')
        root = tree.getroot()

        for channel in root.iter('channel'):
            item = ET.SubElement(channel, 'item')
            id = ET.SubElement(item, 'id')
            id.text = str(flux.id)
            title = ET.SubElement(item, 'title')
            title.text = form.title.data
            description = ET.SubElement(item, 'description')
            description.text = form.description.data
            link = ET.SubElement(item, 'link')
            link.text = form.link.data
            user_id = ET.SubElement(item, 'user_id')
            user_id.text = str(flux.user_id)
        tree.write('main_flux.xml')

        flash('Done')
    return render_template("newflow.html", form=form)


@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')

@app.cli.command()
def fakedata():
    from faker import Faker
    fake = Faker()

    for pk in range(0, 4):
        User.create(name=fake.first_name(),
                      surname=fake.last_name(),
                      email=fake.email(),
                      password=fake.password(length=20, special_chars=True, digits=True, upper_case=True,
                                             lower_case=True))

    # for user in User.select():
    #     for pk in range(0, 5):
    #         name = fake.company()
    #         Flux.create(name=name,
    #                         link=fake.uri(),
    #                         user=user)

